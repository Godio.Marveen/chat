var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
  });

http.listen(3450, function(){
  console.log('listening on *:3450');
});

io.on('connection', function(socket){
  date_heure();
  socket.on('chat message', function(msg){
    io.emit('chat message', msg + resultat);
  });
});

io.emit('some event', { for: 'everyone' });


function date_heure()
{
        date = new Date;
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        resultat = ' à '+h+':'+m+':'+s;
        // document.getElementById(id).innerHTML = resultat;
        setTimeout(date_heure,'1000');
        return true;
}